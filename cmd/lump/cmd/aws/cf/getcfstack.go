package cf

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
)

func init() {
	AWSCFCmd.AddCommand(generateGetCFStackCmd())
}

func generateGetCFStackCmd() *cobra.Command {
	var getCFStackCmd = &cobra.Command{
		Use:   "get [stackName]",
		Short: "Returns a cloud formation stack",
		Long:  `Returns an AWS Cloud Formation stack`,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			getCFStack(args)
		},
	}

	return getCFStackCmd
}

func getCFStack(args []string) {
	awsutil.GetCFStack(args[0])
	/*	keyList, err := awsutil.GetCFStack(args[0])
		if err != nil {
			fmt.Printf("Error retrieving key list: %s", err)
			os.Exit(1)
		}

		for _, keyItem := range keyList {
			fmt.Printf("%s: %s\n", *keyItem.KeyName, *keyItem.KeyFingerprint)
		}
	*/

}
