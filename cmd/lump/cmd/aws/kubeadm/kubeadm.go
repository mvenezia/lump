package kubeadm

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/aws"
)

func init() {
	aws.AWSCmd.AddCommand(AWSKubeadmCmd)
}

var AWSKubeadmCmd = generateAWSKubeadmCmd()

func generateAWSKubeadmCmd() *cobra.Command {
	var createAWSKubeadmCmd = &cobra.Command{
		Use:   "kubeadm",
		Short: "AWS kubeadm commands",
		Long:  `AWS kubeadm Commands`,
	}

	return createAWSKubeadmCmd
}
