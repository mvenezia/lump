package clusterapi

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/aws"
	"io/ioutil"
	"os"
)

func init() {
	AWSClusterAPICmd.AddCommand(generateCreateWorkersCmd())
}

func generateCreateWorkersCmd() *cobra.Command {
	var k8sKeySecret string
	var k8sVersion string
	var usePrivateIPs bool
	var printJSON bool

	var getCreateWorkersCmd = &cobra.Command{
		Use:   "create-workers [stackName] [filename]",
		Short: "Returns a cluster-api machine resource for the workers in the stack",
		Long:  `Returns a cluster-api machine resource for the workers in the stack, filename is optional`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createWorkers(args, k8sVersion, k8sKeySecret, usePrivateIPs, printJSON)
		},
	}
	getCreateWorkersCmd.Flags().StringVar(&k8sVersion, "k8s-version", "1.10.6", "The version of Kubernetes to use")
	getCreateWorkersCmd.Flags().StringVar(&k8sKeySecret, "k8s-key-secret", "", "The Kubernetes secret that has the SSH private key")
	getCreateWorkersCmd.Flags().BoolVar(&usePrivateIPs, "use-private-ips", false, "Whether external or private ips should be used")
	getCreateWorkersCmd.Flags().BoolVar(&printJSON, "json", false, "Set to output in JSON instead of YAML")

	getCreateWorkersCmd.MarkFlagRequired("k8s-key-secret")

	return getCreateWorkersCmd
}

func createWorkers(args []string, k8sVersion string, k8sKeySecret string, usePrivateIPs bool, printJSON bool) {
	stack, parameters, err := awsutil.GetCFStack(args[0])
	if err != nil {
		fmt.Printf("Error retrieving stack: %s", err)
		os.Exit(1)
	}

	data, err := awsutil.GenerateClusterAPIWorkerRecordsHelper(stack, parameters, awsutil.ClusterAPIRecordNonAWSParameters{
		Namespace:        viper.GetString("kubernetes-namespace"),
		K8sVersion:       k8sVersion,
		SSHKeySecretName: k8sKeySecret,
		PrivateAddresses: usePrivateIPs,
		PrintJSON:        printJSON,
	})
	if err != nil {
		fmt.Printf("Error creating master record: %s", err)
		os.Exit(1)
	}
	if len(args) > 1 {
		// Going to dump the json to a file
		ioutil.WriteFile(args[1], data, 0644)
		fmt.Printf("Wrote to file %s\n", args[1])
		return
	}
	fmt.Printf("%s\n", data)
}
