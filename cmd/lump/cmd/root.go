package cmd

import (
	"flag"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"strings"
)

var (
	RootCmd = &cobra.Command{
		Use:              "lump",
		Short:            "The CMA Helper Lump",
		Long:             `The CMA Helper Lump`,
		TraverseChildren: true,
	}
)

func init() {
	viper.SetEnvPrefix("lump")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	RootCmd.PersistentFlags().String("kubeconfig", "", "Location of kubeconfig file")
	RootCmd.PersistentFlags().String("kubectl", "kubectl", "Location of kubectl file")
	RootCmd.PersistentFlags().String("kubernetes-namespace", "default", "What namespace to operate on")

	viper.BindPFlag("kubeconfig", RootCmd.PersistentFlags().Lookup("kubeconfig"))
	viper.BindPFlag("kubectl", RootCmd.PersistentFlags().Lookup("kubectl"))
	viper.BindPFlag("kubernetes-namespace", RootCmd.PersistentFlags().Lookup("kubernetes-namespace"))

	viper.AutomaticEnv()
	RootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
