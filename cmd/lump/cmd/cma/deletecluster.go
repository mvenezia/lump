package cma

import (
	"fmt"
	"github.com/spf13/cobra"
)

func init() {
	CMACmd.AddCommand(generateDeleteClusterCmd())
}

func generateDeleteClusterCmd() *cobra.Command {
	var deleteClusterCmd = &cobra.Command{
		Use:   "delete-cluster",
		Short: "Delete a cluster",
		Long:  `Delete a cluster - stubbed`,
		Run: func(cmd *cobra.Command, args []string) {
			deleteCluster()
		},
	}

	return deleteClusterCmd
}

func deleteCluster() {
	fmt.Print("Hello World, I would have destroyed something\n")
}
