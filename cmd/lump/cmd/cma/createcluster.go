package cma

import (
	"fmt"
	"github.com/spf13/cobra"
)

func init() {
	CMACmd.AddCommand(generateCreateClusterCmd())
}

func generateCreateClusterCmd() *cobra.Command {
	var createClusterCmd = &cobra.Command{
		Use:   "create-cluster",
		Short: "Creates a cluster",
		Long:  `Creates a cluster - stubbed`,
		Run: func(cmd *cobra.Command, args []string) {
			createCluster()
		},
	}

	return createClusterCmd
}

func createCluster() {
	fmt.Print("Hello World; I would have created something\n")
}
