package sshsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	corev1 "k8s.io/api/core/v1"
)

func init() {
	SSHSecretCmd.AddCommand(generateGetSSHSecretCmd())
}

func generateGetSSHSecretCmd() *cobra.Command {
	var generateGetSSHSecretCmd = &cobra.Command{
		Use:   "get [secretName]",
		Short: "Get " + string(corev1.SecretTypeSSHAuth) + " secret",
		Long:  "Get Kubernetes secret of type " + string(corev1.SecretTypeSSHAuth),
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			getSSHSecret(args)
		},
	}

	return generateGetSSHSecretCmd
}

func getSSHSecret(args []string) {
	keyName := args[0]
	secret, err := k8sutil.GetSSHSecret(args[0], viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not get secret %s, reason was %s\n", keyName, err)
		return
	}
	fmt.Printf("Private Key: \n")
	fmt.Printf("%s\n", secret.Data[corev1.SSHAuthPrivateKey])
}
