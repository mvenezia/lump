package sshsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	corev1 "k8s.io/api/core/v1"
)

func init() {
	SSHSecretCmd.AddCommand(generateDeleteSSHSecretCmd())
}

func generateDeleteSSHSecretCmd() *cobra.Command {
	var generateDeleteSSHSecretCmd = &cobra.Command{
		Use:   "delete [secretName]",
		Short: "Delete " + string(corev1.SecretTypeSSHAuth) + "secret",
		Long:  "Delete Kubernetes secret of type " + string(corev1.SecretTypeSSHAuth),
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteSSHSecret(args)
		},
	}

	return generateDeleteSSHSecretCmd
}

func deleteSSHSecret(args []string) {
	keyName := args[0]
	err := k8sutil.DeleteSSHSecret(keyName, viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not delete secret %s, reason was %s\n", keyName, err)
		return
	}
	fmt.Printf("Key -->%s<-- deleted\n", keyName)
}
