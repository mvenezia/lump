package kubeconfigsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
)

func init() {
	KubeconfigSecretCmd.AddCommand(generateListKubeconfigSecretCmd())
}

func generateListKubeconfigSecretCmd() *cobra.Command {
	var generateListKubeconfigSecretCmd = &cobra.Command{
		Use:   "list",
		Short: "Lists all kubeconfig secrets",
		Long:  "Lists all Kubernetes secrets of type kubeconfig",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			listKubeconfigSecrets(args)
		},
	}

	return generateListKubeconfigSecretCmd
}

func listKubeconfigSecrets(args []string) {
	secrets, err := k8sutil.GetKubeconfigSecretList(viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not list secrets, reason was %s\n", err)
		return
	}
	fmt.Printf("Secrets:\n")
	for _, secret := range secrets {
		fmt.Printf("\t%s\n", secret.Name)
	}
}
