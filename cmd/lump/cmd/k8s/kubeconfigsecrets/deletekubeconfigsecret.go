package kubeconfigsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
)

func init() {
	KubeconfigSecretCmd.AddCommand(generateDeleteKubeconfigSecretCmd())
}

func generateDeleteKubeconfigSecretCmd() *cobra.Command {
	var generateDeleteKubeconfigSecretCmd = &cobra.Command{
		Use:   "delete [secretName]",
		Short: "Delete kubeconfig secret",
		Long:  "Delete Kubernetes secret of type kubeconfig",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteKubeconfigSecret(args)
		},
	}

	return generateDeleteKubeconfigSecretCmd
}

func deleteKubeconfigSecret(args []string) {
	keyName := args[0]
	err := k8sutil.DeleteKubeconfigSecret(keyName, viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not delete secret %s, reason was %s\n", keyName, err)
		return
	}
	fmt.Printf("Key -->%s<-- deleted\n", keyName)
}
