package kubeconfigsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	"io/ioutil"
)

func init() {
	KubeconfigSecretCmd.AddCommand(generateCreateKubeconfigSecretCmd())
}

func generateCreateKubeconfigSecretCmd() *cobra.Command {
	var generateCreateKubeconfigSecretCmd = &cobra.Command{
		Use:   "create [secretName] [kubeconfigFile]",
		Short: "Creates a kubeconfig secret",
		Long:  "Creates a Kubernetes secret of type kubeconfig",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			createKubeconfigSecret(args)
		},
	}

	return generateCreateKubeconfigSecretCmd
}

func createKubeconfigSecret(args []string) {
	if len(args) != 2 {
		fmt.Printf("Invalid number of arguments, 2 desired, %d given\n", len(args))
	}

	secretName := args[0]
	kubeconfigFileName := args[1]

	fileContents, err := ioutil.ReadFile(kubeconfigFileName)
	if err != nil {
		fmt.Printf("Could not read in private key file -->%s<--, reason was %s\n", kubeconfigFileName, err)
		return
	}
	err = k8sutil.CreateKubeconfigSecret(secretName, viper.GetString("kubernetes-namespace"), fileContents)
	if err != nil {
		fmt.Printf("Could not create secret -->%s<--, reason was %s\n", secretName, err)
	} else {
		fmt.Printf("Was able to create secret -->%s<--\n", secretName)
	}
}
