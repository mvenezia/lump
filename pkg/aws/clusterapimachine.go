package awsutil

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	yaml2 "github.com/ghodss/yaml"
	"gitlab.com/mvenezia/lump/pkg/util/clusterapi"
	"gitlab.com/mvenezia/lump/pkg/util/clusterapi/models/v1alpha1"
)

type ClusterAPIRecordNonAWSParameters struct {
	Namespace        string
	K8sVersion       string
	SSHKeySecretName string
	PrivateAddresses bool
	PrintJSON        bool
}

func GenerateClusterAPIMasterRecordsHelper(resources []*cloudformation.StackResource, parameters []*cloudformation.Parameter, nonAWSParameters ClusterAPIRecordNonAWSParameters) ([]byte, error) {
	for _, item := range resources {
		if *item.LogicalResourceId == "K8sMaster" {
			return GenerateClusterAPIMasterRecords(item, parameters, nonAWSParameters)
		}
	}
	return nil, fmt.Errorf("could not find K8SMaster resource in stack")
}

func GenerateClusterAPIMasterRecords(resource *cloudformation.StackResource, parameters []*cloudformation.Parameter, nonAWSParameters ClusterAPIRecordNonAWSParameters) ([]byte, error) {
	var ipAddress string
	var username string
	for _, j := range parameters {
		if *j.ParameterKey == "username" {
			username = *j.ParameterValue
			break
		}
	}
	masterInstance, err := GetInstanceDetails(*resource.PhysicalResourceId)
	if err != nil {
		return nil, err
	}
	if nonAWSParameters.PrivateAddresses {
		for _, j := range masterInstance.Instances[0].NetworkInterfaces {
			ipAddress = *j.PrivateIpAddress
		}
	} else {
		ipAddress = *masterInstance.Instances[0].PublicIpAddress
	}

	machine := clusterapiutil.GenerateMachine(clusterapiutil.MachineTemplateParameters{
		Namespace:    nonAWSParameters.Namespace,
		Name:         *masterInstance.Instances[0].InstanceId,
		ControlPlane: true,
		K8sVersion:   nonAWSParameters.K8sVersion,
		SSHProviderOptions: clusterapiutil.SSHProviderTemplateParameters{
			Host:       ipAddress,
			Port:       22,
			SecretName: nonAWSParameters.SSHKeySecretName,
			Username:   username,
		},
	})

	return json.Marshal(machine)
}

func GenerateClusterAPIWorkerRecordsHelper(resources []*cloudformation.StackResource, parameters []*cloudformation.Parameter, nonAWSParameters ClusterAPIRecordNonAWSParameters) ([]byte, error) {
	for _, item := range resources {
		if *item.LogicalResourceId == "K8sNodeGroup" {
			return GenerateClusterAPIWorkerRecords(item, parameters, nonAWSParameters)
		}
	}
	return nil, fmt.Errorf("could not find K8sNodeGroup resource in stack")
}

func GenerateClusterAPIWorkerRecords(resource *cloudformation.StackResource, parameters []*cloudformation.Parameter, nonAWSParameters ClusterAPIRecordNonAWSParameters) ([]byte, error) {
	var ipAddress string
	var username string
	for _, j := range parameters {
		if *j.ParameterKey == "username" {
			username = *j.ParameterValue
			break
		}
	}

	workerInstances, err := GetInstancesForASGDetails(*resource.PhysicalResourceId)
	if err != nil {
		return nil, err
	}

	var machines []v1alpha1.Machine

	for _, worker := range workerInstances.Instances {
		if nonAWSParameters.PrivateAddresses {
			for _, j := range worker.NetworkInterfaces {
				ipAddress = *j.PrivateIpAddress
			}
		} else {
			ipAddress = *worker.PublicIpAddress
		}

		machines = append(machines, clusterapiutil.GenerateMachine(clusterapiutil.MachineTemplateParameters{
			Namespace:    nonAWSParameters.Namespace,
			Name:         *worker.InstanceId,
			ControlPlane: false,
			K8sVersion:   nonAWSParameters.K8sVersion,
			SSHProviderOptions: clusterapiutil.SSHProviderTemplateParameters{
				Host:       ipAddress,
				Port:       22,
				SecretName: nonAWSParameters.SSHKeySecretName,
				Username:   username,
			},
		}))
	}

	if nonAWSParameters.PrintJSON {
		return json.Marshal(machines)
	} else {
		var output []byte
		separator := []byte("---\n")
		for _, machine := range machines {
			encoded, err := json.Marshal(machine)
			if err != nil {
				return nil, err
			}
			encoded, err = yaml2.JSONToYAML(encoded)
			output = append(output, encoded...)
			output = append(output, separator...)
		}
		return output, nil
	}
}
