package awsutilmodels

type DemoCFTemplateOptions struct {
	Name             string
	KeyName          string
	Username         string
	InstanceType     string
	DiskSize         string
	AvailabilityZone string
	SSHLocation      string
	K8sNodeCapacity  string
}
