package clusterapiutil

import (
	"encoding/json"
	"gitlab.com/mvenezia/lump/pkg/util/clusterapi/models/v1alpha1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const (
	MachineAPIVersion = "cluster.k8s.io/v1alpha1"
	MachineKind       = "Machine"
)

type MachineTemplateParameters struct {
	Namespace          string
	Name               string
	K8sVersion         string
	SSHProviderOptions SSHProviderTemplateParameters
	ControlPlane       bool
}

func GenerateMachine(options MachineTemplateParameters) v1alpha1.Machine {
	var versions v1alpha1.MachineVersionInfo

	if options.ControlPlane {
		versions = v1alpha1.MachineVersionInfo{
			Kubelet:      options.K8sVersion,
			ControlPlane: options.K8sVersion,
		}
		options.SSHProviderOptions.Roles = []v1alpha1.MachineRole{v1alpha1.MasterRole, v1alpha1.EtcdRole}
	} else {
		versions = v1alpha1.MachineVersionInfo{
			Kubelet: options.K8sVersion,
		}
		options.SSHProviderOptions.Roles = []v1alpha1.MachineRole{v1alpha1.NodeRole}
	}

	providerConfig := GenerateSSHProvider(options.SSHProviderOptions)

	providerData, _ := json.Marshal(providerConfig)
	return v1alpha1.Machine{
		APIVersion: MachineAPIVersion,
		Kind:       MachineKind,
		ObjectMeta: v1.ObjectMeta{Namespace: options.Namespace, Name: options.Name},
		Spec: v1alpha1.MachineSpec{
			ProviderConfig: v1alpha1.ProviderConfig{
				Value: &runtime.RawExtension{
					Raw: providerData,
				},
			},
			Versions: versions,
		},
	}
}
