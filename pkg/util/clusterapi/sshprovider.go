package clusterapiutil

import "gitlab.com/mvenezia/lump/pkg/util/clusterapi/models/v1alpha1"

const (
	SSHProviderConfigAPIVersion = "sshproviderconfig/v1alpha1"
	SSHProviderConfigKind       = "SSHMachineProviderConfig"
)

type SSHProviderTemplateParameters struct {
	Roles      []v1alpha1.MachineRole
	Username   string
	Host       string
	Port       int
	SecretName string
}

func GenerateSSHProvider(options SSHProviderTemplateParameters) v1alpha1.SSHMachineProviderConfig {
	providerConfig := v1alpha1.SSHMachineProviderConfig{
		APIVersion: SSHProviderConfigAPIVersion,
		Kind:       SSHProviderConfigKind,
		Roles:      options.Roles,
		SSHConfig: v1alpha1.SSHConfig{
			Username:   options.Username,
			Host:       options.Host,
			Port:       options.Port,
			SecretName: options.SecretName,
		},
	}
	return providerConfig
}
