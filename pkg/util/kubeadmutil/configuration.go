package kubeadmutil

import (
	"k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1alpha2"
	"k8s.io/kubernetes/pkg/proxy/apis/kubeproxyconfig/v1alpha1"
)

const (
	InitialConfigAPIVersion = "kubeadm.k8s.io/v1alpha1"
	InitialConfigKind       = "MasterConfiguration"
)

type InitialConfigurationParameters struct {
	PodCIDR     string
	ServiceCIDR string
	K8sVersion  string
	DNSDomain   string
	IPAddress   string
}

func GenerateInitialConfiguration(options InitialConfigurationParameters) v1alpha2.MasterConfiguration {
	configuration := v1alpha2.MasterConfiguration{
		API: v1alpha2.API{
			AdvertiseAddress: options.IPAddress,
			BindPort:         443,
		},
		KubernetesVersion: "v" + options.K8sVersion,
		KubeProxy: v1alpha2.KubeProxy{
			Config: &v1alpha1.KubeProxyConfiguration{
				ClusterCIDR: options.PodCIDR,
			},
		},
		Networking: v1alpha2.Networking{
			PodSubnet:     options.PodCIDR,
			ServiceSubnet: options.ServiceCIDR,
			DNSDomain:     options.DNSDomain,
		},
	}
	configuration.APIVersion = InitialConfigAPIVersion
	configuration.Kind = InitialConfigKind
	return configuration
}
